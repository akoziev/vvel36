var sum = 0;
var rp = document.querySelectorAll('div #radios');
var cp = document.querySelectorAll('div #checkboxes');
var option = document.getElementsByName("ListOfProds");
var Cost = 11000;
var string1 = "";
var additionalCost = {
  prod_options: {
    c1: 750,
    c2: 2400,
    c3: 14000,
  },
  prod_properties: {
    r1: 0,
    r2: -1000,
  }
};

function cost(){
  switch(option[0].value)
  {
    case "Intel core i5-7500" :
        Cost = 11000;
        for(let i = 0; i<rp.length; i++)
          { rp[i].style.display = "none"; }
        for(let i = 0; i<cp.length; i++)
          { cp[i].style.display = "none"; }
        break;
    case "Intel core i5-10500" :
        Cost = 12500;
        for(let i = 0; i<rp.length; i++)
          { rp[i].style.display = "inline-block"; }
        for(let i = 0; i<cp.length; i++)
          { cp[i].style.display = "none"; }
        break;
    case "Intel core i7-7700k" :
        Cost = 21000;
        for(let i = 0; i<rp.length; i++)
          { rp[i].style.display = "none"; }
        for(let i = 0; i<cp.length; i++)
          { cp[i].style.display = "inline-block"; }
        break;
  }
}

function price() {
  var q1 = document.getElementsByName("f1");
  var r = document.getElementById("result");
  var q1q2 = q1[0].value.match(/^[0-9]*$/);
  if( q1q2 !== null)
  	{ 
  		q1q2 = q1[0].value * Cost; 
  		sum+=q1q2; 
  		r.innerHTML = q1q2;
  	}
  else { r.innerHTML = "Не корректно введены данные"; }
}

function summary(){
  var r2 = document.getElementById("SummaryResult");
  r2.innerHTML = sum;
}

window.addEventListener('DOMContentLoaded', function (event) {
  console.log("DOM fully loaded and parsed");
  var b = document.getElementById("button");
  var c = document.getElementById("SummaryButton");

  for(let i = 0; i<rp.length; i++)
  { rp[i].style.display = "none"; }
  for(let i = 0; i<cp.length; i++)
  { cp[i].style.display = "none"; }

  option[0].addEventListener("click", cost);
  c.addEventListener("click", summary);
  b.addEventListener("click", price);


  rp.forEach(function(radio){
    radio.addEventListener("click", function(event){
        let tempr = additionalCost.prod_properties[event.target.value];
        if(tempr !== undefined){
          Cost+=tempr;
          price(); cost();
        }
    })
  })


  cp.forEach(function(checkbox){
    checkbox.addEventListener("click", function(event){
      if(event.target.checked){
        let tempc = additionalCost.prod_options[event.target.name];
        if(tempc !== undefined){
          Cost+=tempc;
          price();
        }
      }
      else { let tempc = additionalCost.prod_options[event.target.name];
        if(tempc !== undefined){
          Cost-=tempc;
          price();
        } 
      }
    })
  })


});